package ro.ase.dam.doc;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class ListBooksActivity extends ActionBarActivity {
	public static final int DISPLAY = 0;
	private StringBuilder booksBuilder = new StringBuilder();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_books);
		
		ListView listView = (ListView) findViewById(R.id.listView1); 
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent intent = new Intent(view.getContext(),AboutBookActivity.class);
				intent.putExtra("id-utilizator", 0);
				intent.putExtra("id-carte", 0);
			}
		});
		
		ArrayList<String> books = new ArrayList<String>();
		books.add("CARTE 1");
		books.add("CARTE 2");
		books.add("CARTE 3");
		
		for (String s:books){
			booksBuilder.append(s).append(" ");
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_expandable_list_item_1, books);
		
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.list_books, menu);
		menu.add(0,DISPLAY,0,"List books");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()){
		case DISPLAY:
			Toast.makeText(getApplicationContext(), booksBuilder.toString(), Toast.LENGTH_LONG);
			return true;
		}
		return false;
	}
}
