package ro.ase.dam.doc;

import ro.ase.dam.doc.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class HomeActivity extends Activity {

	private static final int idUtilizator = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void openAboutActivity(View view){
		Intent aboutIntent = new Intent(this,AboutActivity.class);
		aboutIntent.putExtra("id-utilizator", idUtilizator);
		startActivity(aboutIntent);
	}
	
	public void openAddBookActivity(View view){
		Intent addBookIntent = new Intent(this,AddBookActivity.class);
		addBookIntent.putExtra("id-utilizator", idUtilizator);
		startActivity(addBookIntent);
	}
	
	public void openListBooksActivity(View view){
		Intent listBookIntent = new Intent(this,ListBooksActivity.class);
		listBookIntent.putExtra("id-utilizator", idUtilizator);
		startActivity(listBookIntent);
	}
	
	public void openWishlistActivity(View view){
		Intent wishlistButton = new Intent(this,WishlistActivity.class);
		wishlistButton.putExtra("id-utilizator", idUtilizator);
		startActivity(wishlistButton);
	}
	
}
